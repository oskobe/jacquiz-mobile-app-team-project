package ca.ipd12.jing.jacquiz;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ca.ipd12.jing.jacquiz.Models.QuestionDetail;
import ca.ipd12.jing.jacquiz.Utilities.OptionMenuUtility;

public class QuizResultActivity extends OptionMenuUtility {

    // Variable declaration
    TextView tv_result;
    List<QuestionDetail> questionDetailList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);

        // Set title (with question number / total question)
        setTitle("Quiz Result");

        Initializtion();
    }

    private void Initializtion() {
        // Intent declaration
        Intent receivedIntent = getIntent();
        // Final grade declaration
        int finalGrade;
        String resultStr;

        // Get final grade from receivedIntent
        if (receivedIntent.hasExtra("finalGrade")) {
            finalGrade = receivedIntent.getIntExtra("finalGrade", 0);
            //Log.i("FinalResult", finalGrade + "%");
            // Set final result string to the quiz result text view
            if (finalGrade >= 80) {
                resultStr = getString(R.string.result_good) + " " + finalGrade + getString(R.string.percent);
            } else {
                resultStr = getString(R.string.result_ng) + " " + finalGrade + getString(R.string.percent);
            }

            tv_result = findViewById(R.id.tv_result);
            tv_result.setText(resultStr);
        }

        // Get questionDetails list from Extra
        if (receivedIntent.hasExtra("questionDetailsList")){
            questionDetailList = (List<QuestionDetail>)receivedIntent.getSerializableExtra("questionDetailsList");
            //Log.i("EXTRA", questionDetailList.toString());
        }

    }

    public void btnStartQuizOnClick(View view) {
        // Go to start a quiz page
        Intent startQuizIntent = new Intent(QuizResultActivity.this, MainActivity.class);

        startActivity(startQuizIntent);
    }

    public void btnReivewAnswersOnClick(View view) {
        // Go to answer review page
        Intent answerReviewIntent = new Intent(QuizResultActivity.this, ReviewAnswerActivity.class);

        // put questionDetails list to Extra
        answerReviewIntent.putExtra("questionDetailsList", (Serializable) questionDetailList);

        startActivity(answerReviewIntent);
    }
}
