package ca.ipd12.jing.jacquiz.Models;

import java.util.HashMap;
import java.util.Map;

public class QuizOptions {
    public static final String ANY = "any";
    public static final String DEFAULT_NUMBER_OF_QUESTION = "10";

    public static HashMap<String, String> categoryMap = new HashMap<String, String>(){
        {
            put("Any Category", "any");
            put("General Knowledge", "9");
            put("Entertainment: Books", "10");
            put("Entertainment: Film", "11");
            put("Entertainment: Music", "12");
            put("Entertainment: Musicals, Theatres", "13");
            put("Entertainment: Television", "14");
            put("Entertainment: Video Games", "15");
            put("Entertainment: Board Games", "16");
            put("Science: Nature", "17");
            put("Science: Computers", "18");
            put("Science: Mathematics", "19");
            put("Mythology", "20");
            put("Sports", "21");
            put("Geography", "22");
            put("History", "23");
            put("Politics", "24");
            put("Art", "25");
            put("Celebrities", "26");
            put("Animals", "27");
            put("Vehicles", "28");
            put("Entertainment: Comics", "29");
            put("Science: Gadgets", "30");
            put("Entertainment: Japanese Anime, Manga", "31");
            put("Entertainment: Cartoon, Animations", "32");
        }
    };
    public static HashMap<String, String> difficultyMap = new HashMap<String, String>(){
        {
            put("Any Difficulty", "any");
            put("Easy", "easy");
            put("Medium", "medium");
            put("Hard", "hard");
        }
    };
    public static HashMap<String, String> typeMap = new HashMap<String, String>() {
        {
            put("Any Type", "any");
            put("Multiple Choice", "multiple");
            put("True / False", "boolean");
        }
    };

}
