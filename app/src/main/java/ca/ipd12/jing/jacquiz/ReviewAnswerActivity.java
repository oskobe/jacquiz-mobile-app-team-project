package ca.ipd12.jing.jacquiz;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ca.ipd12.jing.jacquiz.Models.QuestionDetail;
import ca.ipd12.jing.jacquiz.Utilities.OptionMenuUtility;

public class ReviewAnswerActivity extends OptionMenuUtility {

    // Variable declaration
    List<QuestionDetail> questionDetailList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_answer);

        mRecyclerView = findViewById(R.id.result_recycler_view);
        mRecyclerView.setHasFixedSize(true);


        // Set title
        setTitle("Review your Answers");

        // Get data source (question details list)
        initialization();
    }
//Create a Adapter for display result in a recycleview.
    public class MyResultAdapter extends RecyclerView.Adapter<MyResultAdapter.ViewHolder> {
        private List<QuestionDetail> dataSourceList;//hold data
        private Context context;//get applicationcontext

        public MyResultAdapter(Context context, List<QuestionDetail> dataSourceList) {
            this.dataSourceList=dataSourceList;
            this.context=context;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            // create a new view from customed item layout
            RelativeLayout v = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.custom_review_item, viewGroup, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
            QuestionDetail questionDetail = dataSourceList.get(i);
            if(questionDetail.getYourAnswerCorrect()){
                viewHolder.answer.setText("Your Answer: "+questionDetail.getAnswer());
            }else {
                viewHolder.answer.setText("Your Answer: "+questionDetail.getYourAnswer());
                viewHolder.answer.setTextColor(Color.parseColor("#FF0000"));
            }
            viewHolder.correct.setText("Correct Answer: "+questionDetail.getAnswer());
            viewHolder.question.setText("Question"+(i+1)+": "+questionDetail.getQuestion());
        }

        @Override
        public int getItemCount() {
            return dataSourceList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            //View holder three textview to display data
            private TextView question;
            private TextView correct;
            private TextView answer;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                question = itemView.findViewById(R.id.tvQuestion);
                correct = itemView.findViewById(R.id.tvCorrect);
                answer = itemView.findViewById(R.id.tvAnswer);
            }
        }
    }

    private void initialization() {
        // Intent declaration
        Intent receivedIntent = getIntent();

        // Get questionDetails list from Extra
        if (receivedIntent.hasExtra("questionDetailsList")) {
            questionDetailList = (List<QuestionDetail>) receivedIntent.getSerializableExtra("questionDetailsList");
            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
// specify an adapter (see also next example)
            mAdapter = new MyResultAdapter(this,questionDetailList);
            mRecyclerView.setAdapter(mAdapter);
            //Log.i("EXTRA", questionDetailList.toString());
        }
    }
}
