package ca.ipd12.jing.jacquiz.Utilities;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Date;
import ca.ipd12.jing.jacquiz.Models.QuizResult;
import ca.ipd12.jing.jacquiz.R;

public class SendQuizResultEmailAsyncTask extends AsyncTask<QuizResult,Void,String> {
//This class is for sending email onBackground
    private Context context;

    public SendQuizResultEmailAsyncTask(Context context) {
        this.context = context;
    }

    //Check If your device has internet Connection
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        //No matter what is the result, This function just for user
        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(QuizResult... QuizResults) {
        if(isOnline()){
            int count = QuizResults.length;
            for (int i = 0; i < count; i++) {
                QuizResult quizResult = QuizResults[i];
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    System.out.println(formatter.format(date));
                    String subject = "[JacQuizResult-"+formatter.format(date)+"]";
                    GMailSender sender = new GMailSender(
                            context.getString(R.string.sender_gmail_user),
                            context.getString(R.string.sender_gmail_password));
                    sender.sendMail(subject, "Hi Friend,\n"+
                                    "Your result of quiz is "+quizResult.getGrade()+"%.\n"+
                                    "Good Luck!\n\n"+
                                    "JacQuiz Center .Inc",
                            context.getString(R.string.sender_gmail_user),
                            quizResult.getEmail());
                } catch (Exception e) {
                    Log.i("Sending Email Error", e.getMessage());
                }
                if (isCancelled()) break;
            }
            return context.getString(R.string.result_send_response);
        }else{
            return "Your device has no Internet!";
        }

    }
}
