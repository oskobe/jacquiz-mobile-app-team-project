package ca.ipd12.jing.jacquiz.Models;

import android.provider.BaseColumns;

import java.io.Serializable;

public class QuizResult implements Serializable {
    private Long id;
    private String email;
    private int grade;

    public QuizResult(String email, int grade) {
        this.email = email;
        this.grade = grade;
    }

    public QuizResult(Long id, String email, int grade) {
        this.id = id;
        this.email = email;
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public static final String SQL_CREATE_QUIZRESULT_ENTRIES =
            "CREATE TABLE " + QuizResultEntry.TABLE_NAME + " (" +
                    QuizResultEntry._ID + " INTEGER PRIMARY KEY," +
                    QuizResultEntry.COLUMN_NAME_EMAIL + " TEXT," +
                    QuizResultEntry.COLUMN_NAME_GRADE + " INTEGER)";

    public static final String SQL_DELETE_QUIZRESULT_ENTRIES =
            "DROP TABLE IF EXISTS " + QuizResultEntry.TABLE_NAME;


    public static class QuizResultEntry implements BaseColumns {
        public static final String TABLE_NAME = "QuizResults";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_GRADE = "grade";
    }
}
