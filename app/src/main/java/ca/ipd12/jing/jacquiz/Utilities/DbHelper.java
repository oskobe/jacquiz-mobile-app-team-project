package ca.ipd12.jing.jacquiz.Utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

import ca.ipd12.jing.jacquiz.Models.QuizResult;


public class DbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;// SQLite database version
    public static final String DATABASE_NAME = "Project.db"; //Database Name

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //Log.i("TestDBhelper", "DbHelper: ");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QuizResult.SQL_CREATE_QUIZRESULT_ENTRIES);
        //Log.i("TestDBhelper", "onCreate: ");
        //quizResultsSeed();// Better just execute once
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.i("TestDBhelper", "onUpgrade: ");
        db.execSQL(QuizResult.SQL_DELETE_QUIZRESULT_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.i("TestDBhelper", "onDowngrade: ");
        onUpgrade(db, oldVersion, newVersion);
    }

    //For getting a int between two number
    private int randomInteger(int minimum, int maximum) {
        Random rand = new Random();
        return rand.nextInt((maximum - minimum) + 1) + minimum;
    }

    //This function just need to execute once to insert some test data
    public void quizResultsSeed() {
        String initEmailUserName = "SuperHero";
        String email;
        int grade;
        for (int i = 1; i < 33; i++) {
            if (i < 10) {
                email = initEmailUserName + "0" + i + "@jac.com";
            } else {
                email = initEmailUserName + i + "@jac.com";
            }
            grade = randomInteger(0, 10) * 10;// Generate 0 10 20 30....100 grades
            QuizResult qr = new QuizResult(email, grade);
            newRecordToDb(QuizResult.QuizResultEntry.TABLE_NAME, quizResultRecordFactory(qr));
        }
    }
//for create a ContentValues from a QuizResult object
    public ContentValues quizResultRecordFactory(QuizResult newQuiz) {
        ContentValues values = new ContentValues();
        values.put(QuizResult.QuizResultEntry.COLUMN_NAME_EMAIL, newQuiz.getEmail());
        values.put(QuizResult.QuizResultEntry.COLUMN_NAME_GRADE, newQuiz.getGrade());
        return values;
    }
//insert any Contentvalues into specific table
    public void newRecordToDb(String tableName, ContentValues newRecord) {
        long newRowId = this.getWritableDatabase().insertOrThrow(tableName, null, newRecord);
        //Log.i("NewRecord", "newRecordToDb:RowID " + newRowId);
    }

    public ArrayList<QuizResult> getAllQuizResultRecordsOrderByGradeDesc() {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] Columns = {
                QuizResult.QuizResultEntry._ID,
                QuizResult.QuizResultEntry.COLUMN_NAME_EMAIL,
                QuizResult.QuizResultEntry.COLUMN_NAME_GRADE,
        };
        String sortOrder = QuizResult.QuizResultEntry.COLUMN_NAME_GRADE + " DESC";
        Cursor cursor = db.query(
                QuizResult.QuizResultEntry.TABLE_NAME,   // The table to query
                Columns,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        ArrayList<QuizResult> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(QuizResult.QuizResultEntry._ID));
            String itemEmail = cursor.getString(cursor.getColumnIndexOrThrow(QuizResult.QuizResultEntry.COLUMN_NAME_EMAIL));
            int itemGrade = cursor.getInt(cursor.getColumnIndexOrThrow(QuizResult.QuizResultEntry.COLUMN_NAME_GRADE));
            QuizResult u = new QuizResult(itemId, itemEmail, itemGrade);
            list.add(u);
        }
        cursor.close();
        return list;
    }
}
