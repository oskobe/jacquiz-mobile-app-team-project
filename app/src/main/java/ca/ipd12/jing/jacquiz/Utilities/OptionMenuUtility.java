package ca.ipd12.jing.jacquiz.Utilities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.io.Serializable;

import ca.ipd12.jing.jacquiz.MainActivity;
import ca.ipd12.jing.jacquiz.QuizHistoryActivity;
import ca.ipd12.jing.jacquiz.R;

public class OptionMenuUtility extends AppCompatActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public void miQuizHistoryOnClick(MenuItem item) {
        DbHelper db =new DbHelper(OptionMenuUtility.this);
        //db.quizResultsSeed();
        Intent quizHistoryIntent = new Intent(OptionMenuUtility.this, QuizHistoryActivity.class);
        try{
            quizHistoryIntent.putExtra("HISTORY_LIST",db.getAllQuizResultRecordsOrderByGradeDesc());
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            db.close();
        }
        startActivity(quizHistoryIntent);
    }

    public void miStartQuizOnclick(MenuItem item) {
        Intent mainActivityIntent = new Intent(OptionMenuUtility.this, MainActivity.class);
        startActivity(mainActivityIntent);
    }
}
