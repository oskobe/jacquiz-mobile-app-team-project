package ca.ipd12.jing.jacquiz;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import ca.ipd12.jing.jacquiz.Models.QuizOptions;
import ca.ipd12.jing.jacquiz.Utilities.NetworkUtinity;
import ca.ipd12.jing.jacquiz.Utilities.OptionMenuUtility;

public class MainActivity extends OptionMenuUtility implements AdapterView.OnItemSelectedListener {

    // Email edit text
    EditText et_email;
    // Process dialog
    private Dialog progressDialog;

    // Quiz options
    private String numberOfQuestions = QuizOptions.DEFAULT_NUMBER_OF_QUESTION;
    private String questionCategory = QuizOptions.ANY;
    private String questionDifficulty = QuizOptions.ANY;
    private String questionType = QuizOptions.ANY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set title
        setTitle("JAC Quiz");

        // Set select options for QUIZ OPTION
        setSpinner();
    }

    //Check If your device has internet Connection
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public void btnAnswerQuizOnClick(View view) {
        if(!isOnline()){
            Toast.makeText(MainActivity.this, "Your device has not internet", Toast.LENGTH_LONG).show();
            return;
        }
        // Declare Url variable
        URL apiUrl;



        // Get email by id
        et_email = findViewById(R.id.et_email);

        // Email validation
        if (!isValidEmail(et_email.getText())) {
            Toast.makeText(MainActivity.this, "Invalid email", Toast.LENGTH_LONG).show();
            return;
        }

        // Show progress dialog
        showProgressDialog("Loading");

        // Fetch questions and answers from API
        // Set apiUrl
        apiUrl = setUrl();

        // Fetch data by calling api
        new FetchDataFromApi().execute(apiUrl);


    }

    private URL setUrl() {
        URL apiUrl;
        String baseUrl = "https://opentdb.com/api.php?amount=";

        // Set question quantity
        String UrlForApi = baseUrl + numberOfQuestions;

        // Set question category
        if (!questionCategory.equals(QuizOptions.ANY)) {
            UrlForApi += "&category=" + questionCategory;
        }

        // Set question difficulty
        if (!questionDifficulty.equals(QuizOptions.ANY)) {
            UrlForApi += "&difficulty=" + questionDifficulty;
        }

        // Set question type
        if (!questionType.equals(QuizOptions.ANY)) {

            UrlForApi += "&type=" + questionType;
        }

        // Generate api Url
        try {
            apiUrl = new URL(UrlForApi);
            //Log.i("URL", apiUrl.toString());
            return apiUrl;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * // Email validation
     * @param target
     * @return
     */
    private boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public class FetchDataFromApi extends AsyncTask<URL, Void, String> {

        @Override
        protected String doInBackground(URL... urls) {
            String response = "";
            URL myUrl = urls[0];
            try {
                response = NetworkUtinity.getResponseFromHttpUrl(myUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            // Initialize intent
            Intent answerQuizIntent = new Intent(MainActivity.this, AnswerQuizActivity.class);

            // Set Extra: email, quiz string, and quiz options which will be passed to next activity
            answerQuizIntent.putExtra("email", et_email.getText().toString());
            answerQuizIntent.putExtra("quizListJsonString", s);

            // Close progress dialog
            //progressDialog.dismiss();

            // Start answer quiz activity
            startActivity(answerQuizIntent);
        }
    }

    /**
     * Set select options for QUIZ OPTION
     */
    private void setSpinner(){
        // Get spinner by id
        Spinner sp_number = findViewById(R.id.sp_number_of_questions);
        Spinner sp_category =  findViewById(R.id.sp_category);
        Spinner sp_difficulty =  findViewById(R.id.sp_difficulty);
        Spinner sp_type =  findViewById(R.id.sp_type);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter_number = ArrayAdapter.createFromResource(this,
                R.array.array_number_of_question, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter_category = ArrayAdapter.createFromResource(this,
                R.array.array_category, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter_difficulty = ArrayAdapter.createFromResource(this,
                R.array.array_difficulty, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter_type = ArrayAdapter.createFromResource(this,
                R.array.array_type, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter_number.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter_category.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter_difficulty.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        sp_number.setAdapter(adapter_number);
        sp_category.setAdapter(adapter_category);
        sp_difficulty.setAdapter(adapter_difficulty);
        sp_type.setAdapter(adapter_type);

        // Set event listener
        sp_number.setOnItemSelectedListener(this);
        sp_category.setOnItemSelectedListener(this);
        sp_difficulty.setOnItemSelectedListener(this);
        sp_type.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        //Log.i("RESULT", adapterView.getItemAtPosition(i).toString());

        // Get clicked spinner id
        int clickedSpinnerID = adapterView.getId();

        // Get quiz options
        switch (clickedSpinnerID) {
            case R.id.sp_number_of_questions:
                numberOfQuestions = adapterView.getItemAtPosition(i).toString();
                //Log.i("RESULT", "question number " + numberOfQuestions);
                break;
            case R.id.sp_category:
                questionCategory = QuizOptions.categoryMap.get(adapterView.getItemAtPosition(i).toString());
                //Log.i("RESULT", "question category " + questionCategory);
                break;
            case R.id.sp_difficulty:
                questionDifficulty = QuizOptions.difficultyMap.get(adapterView.getItemAtPosition(i).toString());
                //Log.i("RESULT", "question difficulty " + questionDifficulty);
                break;
            case R.id.sp_type:
                questionType = QuizOptions.typeMap.get(adapterView.getItemAtPosition(i).toString());
                //Log.i("RESULT", "question type " + questionType);
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    /**
     * Show progress dialog
     * @param message
     */
    public void showProgressDialog(String message) {
        progressDialog = new Dialog(MainActivity.this, R.style.progress_dialog);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setCancelable(true);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView tvMessage = progressDialog.findViewById(R.id.tv_loadingMsg);
        tvMessage.setText(message);
        progressDialog.show();
    }

}
