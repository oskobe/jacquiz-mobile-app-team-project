package ca.ipd12.jing.jacquiz.Models;

import java.io.Serializable;
import java.util.List;

public class QuestionDetail implements Serializable {
    String question;
    String answer;
    Boolean isYourAnswerCorrect;
    String yourAnswer;
    List<String> choices;

    // Constructor without parameter
    public QuestionDetail() {

    }

    // Construction with parameters
    public QuestionDetail(String question, String answer, List<String> choices) {
        this.question = question;
        this.answer = answer;
        this.choices = choices;
    }

    public QuestionDetail(String question, String answer, Boolean isYourAnswerCorrect, String yourAnswer, List<String> choices) {
        this.question = question;
        this.answer = answer;
        this.isYourAnswerCorrect = isYourAnswerCorrect;
        this.yourAnswer = yourAnswer;
        this.choices = choices;
    }

    // Getters & Setters
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

    public String getYourAnswer() {
        return yourAnswer;
    }

    public void setYourAnswer(String yourAnswer) {
        this.yourAnswer = yourAnswer;
    }

    public Boolean getYourAnswerCorrect() {
        return isYourAnswerCorrect;
    }

    public void setYourAnswerCorrect(Boolean yourAnswerCorrect) {
        isYourAnswerCorrect = yourAnswerCorrect;
    }
}
