package ca.ipd12.jing.jacquiz;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ca.ipd12.jing.jacquiz.Models.QuestionDetail;
import ca.ipd12.jing.jacquiz.Models.QuizResult;
import ca.ipd12.jing.jacquiz.Utilities.DbHelper;
import ca.ipd12.jing.jacquiz.Utilities.OptionMenuUtility;
import ca.ipd12.jing.jacquiz.Utilities.SendQuizResultEmailAsyncTask;

public class AnswerQuizActivity extends OptionMenuUtility {

    // Variables declaration
    String email;
    int totalQuantityOfQuestions = 0;
    int quantityOfCorrectAnswers = 0;
    int currentQuestion = 0;
    TextView tv_question;
    Button btn_answer0, btn_answer1, btn_answer2, btn_answer3;
    DbHelper dbHelper;

    List<QuestionDetail> questionDetailList = new ArrayList<>();


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentQuestion", currentQuestion);
        outState.putInt("quantityOfCorrectAnswers", quantityOfCorrectAnswers);
        outState.putSerializable("questionDetailList", (Serializable) questionDetailList);
        outState.putInt("totalQuantityOfQuestions", totalQuantityOfQuestions);
        outState.putString("email", email);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_quiz);

        // Variables declaration
        Intent receivedIntent = getIntent();
        String quizListJsonString;



        // In the case of rotation
        if (savedInstanceState != null) {
            currentQuestion = savedInstanceState.getInt("currentQuestion");
            quantityOfCorrectAnswers = savedInstanceState.getInt("quantityOfCorrectAnswers");
            questionDetailList = (List<QuestionDetail>) savedInstanceState.getSerializable("questionDetailList");
            totalQuantityOfQuestions = savedInstanceState.getInt("totalQuantityOfQuestions");
            email = savedInstanceState.getString("email");

        }
        // Initialization
        else {

            // Check if email Extra exists
            if (receivedIntent.hasExtra("email")) {
                email = receivedIntent.getStringExtra("email");
            }

            // Check if quiz list Json string exists
            if (receivedIntent.hasExtra("quizListJsonString")) {
                // Get quiz list Json string
                quizListJsonString = receivedIntent.getStringExtra("quizListJsonString");

                try {

                    // Parse string to Json Object
                    JSONObject responseObj = new JSONObject(quizListJsonString);

                    // Case: correct response
                    if (responseObj.getString("response_code").equals("0")) {
                        // Fetch question array
                        JSONArray questionsArray = responseObj.getJSONArray("results");

                        // Total question quantity
                        totalQuantityOfQuestions = questionsArray.length();

                        // Loop and convert json data to question object
                        for (int i = 0; i < totalQuantityOfQuestions; i++) {

                            // Variables declaration
                            String question;
                            String answer;
                            List<String> choices = new ArrayList<>(); // all selectable choices

                            // Get json format question object
                            JSONObject jsonQuestionObject = questionsArray.getJSONObject(i);

                            // Get question
                            question = jsonQuestionObject.getString("question");
                            // Get answer
                            answer = jsonQuestionObject.getString("correct_answer");

                            // Get all incorrect answers
                            JSONArray incorrectAnswersArray = jsonQuestionObject.getJSONArray("incorrect_answers");

                            // Put correct & incorrect answers to Choice list
                            // Correct answer
                            choices.add(answer);

                            // IncorrectAnswers
                            for (int j = 0; j < incorrectAnswersArray.length(); j++) {
                                choices.add(incorrectAnswersArray.getString(j));
                            }

                            // Shuffle the choices randomly
                            Collections.shuffle(choices);

                            // Create a new question object and put it in the list
                            QuestionDetail questionDetail = new QuestionDetail(question, answer, choices);

                            questionDetailList.add(questionDetail);

                        }

                    }


                } catch (Throwable tx) {
                    Log.e("My App", "Could not parse malformed JSON: \"" + quizListJsonString + "\"");
                }
            }
        }

        // Create Dbhelper instance
        dbHelper = new DbHelper(AnswerQuizActivity.this);
        // Receive Extras and parse Json String to Question object
        //initialization();

        // Display first question
        displayQuestion(currentQuestion);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Close Dbhelper
        dbHelper.close();
    }



    /**
     * Button onClick event
     *
     * @param view
     */
    public void btnAnswerOnClick(View view) {

        // Get button id, then obtain button text, which was the answer user chose
        int btnID = view.getId();
        Button btnClicked = findViewById(btnID);
        String youAnswer = btnClicked.getText().toString();

        // When correct answer
        if (youAnswer.equals((Html.fromHtml(questionDetailList.get(currentQuestion).getAnswer(), Html.FROM_HTML_MODE_LEGACY)).toString())) {
            // Add 1 mark
            quantityOfCorrectAnswers++;

            // Set yourAnswer to true
            questionDetailList.get(currentQuestion).setYourAnswerCorrect(true);
        }
        else {
            // Set yourAnswer to false
            questionDetailList.get(currentQuestion).setYourAnswerCorrect(false);

            // Set your answer (incorrect) to question details
            questionDetailList.get(currentQuestion).setYourAnswer(youAnswer);
        }

        // Submit the result When finishing the last question
        if (currentQuestion == totalQuantityOfQuestions - 1) {
            //Log.i("POINT", quantityOfCorrectAnswers + " / " + totalQuantityOfQuestions);

            // Grade Convert to per cent
            int finalGrade = (int) (Math.ceil(quantityOfCorrectAnswers * 100 / totalQuantityOfQuestions));


            // Insert a new record to DB
            QuizResult quizResultObject = new QuizResult(email, finalGrade);
            dbHelper.newRecordToDb(QuizResult.QuizResultEntry.TABLE_NAME, dbHelper.quizResultRecordFactory(quizResultObject));


            // Send Email

            new SendQuizResultEmailAsyncTask(getApplicationContext()).execute(quizResultObject);


            ///////////////////////////

            // Go to quiz result page
            Intent quizResultIntent = new Intent(AnswerQuizActivity.this, QuizResultActivity.class);

            // Put result to Extra
            quizResultIntent.putExtra("finalGrade", finalGrade);

            // put questionDetails list to Extra
            quizResultIntent.putExtra("questionDetailsList", (Serializable) questionDetailList);

            //Log.i("FinalResult", finalGrade + "%");
            startActivity(quizResultIntent);
            return;
        }

        // Move to next question
        currentQuestion++;
        displayQuestion(currentQuestion);

    }

    /**
     * display current question
     *
     * @param idx: the index of question
     */
    private void displayQuestion(int idx) {

        // Get current question object
        QuestionDetail qd = questionDetailList.get(idx);

        // Get all controls by id
        tv_question = findViewById(R.id.tv_question);
        btn_answer0 = findViewById(R.id.btn_answer0);
        btn_answer1 = findViewById(R.id.btn_answer1);
        btn_answer2 = findViewById(R.id.btn_answer2);
        btn_answer3 = findViewById(R.id.btn_answer3);

        // Set title (with question number / total question)
        setTitle("Answering Quiz " + (idx + 1) + " / " + totalQuantityOfQuestions);
        // Set question for text view
        tv_question.setText(Html.fromHtml(qd.getQuestion(), Html.FROM_HTML_MODE_LEGACY));

        // Hide last 2 buttons (in case of True/False questions, which only have 2 answers)
        btn_answer2.setVisibility(View.INVISIBLE);
        btn_answer3.setVisibility(View.INVISIBLE);
        // Set answers for the button
        for (int i = 0; i < qd.getChoices().size(); i++) {
            String choice = qd.getChoices().get(i);
            switch (i) {
                case 0:
                    btn_answer0.setText(Html.fromHtml(choice, Html.FROM_HTML_MODE_LEGACY));
                    break;
                case 1:
                    btn_answer1.setText(Html.fromHtml(choice, Html.FROM_HTML_MODE_LEGACY));
                    break;
                case 2:
                    btn_answer2.setVisibility(View.VISIBLE);
                    btn_answer2.setText(Html.fromHtml(choice, Html.FROM_HTML_MODE_LEGACY));
                    break;
                case 3:
                    btn_answer3.setVisibility(View.VISIBLE);
                    btn_answer3.setText(Html.fromHtml(choice, Html.FROM_HTML_MODE_LEGACY));
                    break;
                default:
                    break;
            }
        }

    }

    /**
     * Receive Extras and parse Json String to Question object
     */
    private void initialization() {
        // Variables declaration
        Intent receivedIntent = getIntent();
        String quizListJsonString;

        // Check if email Extra exists
        if (receivedIntent.hasExtra("email")) {
            email = receivedIntent.getStringExtra("email");
        }

        //if (questionDetailList == null) {

//            // Check if quiz list Json string exists
//            if (receivedIntent.hasExtra("quizListJsonString")) {
//                // Get quiz list Json string
//                quizListJsonString = receivedIntent.getStringExtra("quizListJsonString");
//
//                try {
//
//                    // Parse string to Json Object
//                    JSONObject responseObj = new JSONObject(quizListJsonString);
//
//                    // Case: correct response
//                    if (responseObj.getString("response_code").equals("0")) {
//                        // Fetch question array
//                        JSONArray questionsArray = responseObj.getJSONArray("results");
//
//                        // Total question quantity
//                        totalQuantityOfQuestions = questionsArray.length();
//
//                        // Loop and convert json data to question object
//                        for (int i = 0; i < totalQuantityOfQuestions; i++) {
//
//                            // Variables declaration
//                            String question;
//                            String answer;
//                            List<String> choices = new ArrayList<>(); // all selectable choices
//
//                            // Get json format question object
//                            JSONObject jsonQuestionObject = questionsArray.getJSONObject(i);
//
//                            // Get question
//                            question = jsonQuestionObject.getString("question");
//                            // Get answer
//                            answer = jsonQuestionObject.getString("correct_answer");
//
//                            // Get all incorrect answers
//                            JSONArray incorrectAnswersArray = jsonQuestionObject.getJSONArray("incorrect_answers");
//
//                            // Put correct & incorrect answers to Choice list
//                            // Correct answer
//                            choices.add(answer);
//
//                            // IncorrectAnswers
//                            for (int j = 0; j < incorrectAnswersArray.length(); j++) {
//                                choices.add(incorrectAnswersArray.getString(j));
//                            }
//
//                            // Shuffle the choices randomly
//                            Collections.shuffle(choices);
//
//                            // Create a new question object and put it in the list
//                            QuestionDetail questionDetail = new QuestionDetail(question, answer, choices);
//
//                            questionDetailList.add(questionDetail);
//
//                        }
//
//                    }
//
//
//                } catch (Throwable tx) {
//                    Log.e("My App", "Could not parse malformed JSON: \"" + quizListJsonString + "\"");
//                }
//            }
        //}



    }


}
