package ca.ipd12.jing.jacquiz;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;

//import android.support.v7.widget.ActivityChooserModel;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ca.ipd12.jing.jacquiz.Models.QuizResult;
import ca.ipd12.jing.jacquiz.Utilities.DbHelper;
import ca.ipd12.jing.jacquiz.Utilities.OptionMenuUtility;

public class QuizHistoryActivity extends OptionMenuUtility {
    private ListView listView;
    private Button previous, next;
    private TextView tvEmail, tvDisplayPageNumber;
    private int lastPage, currentPage = 0;
    private CustomListViewAdapter customListViewAdapter;
    private ConstraintLayout historyContraintLayout;
    private final int FIRST_PLACE = 0, SECOND_PLACE = 1, THIRD_PLACE = 2;
    private int availableListViewHeightInDp;
    private double bestItemHeightInDp;
    private int currentDPI;
    private int currentOrientation;
    private final int BASELINE_MDPI = 160, FIRST_PAGE = 0;
    private final int VERTICAL_ITEMS_NUMBER = 10, HORITAL_ITEMS_NUMBER = 3;
    private final int FLAT_VERTICAL = 1, FLAG_HORITAL = 2;
    private final int LISTVIEW_MARGIN_PX = 16;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_history);
        previous = findViewById(R.id.btnPrevious);
        next = findViewById(R.id.btnNext);
//next button event
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPage++;
                updateData();
            }
        });
//previous button event
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPage--;
                updateData();
            }
        });

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        currentDPI = metrics.densityDpi;//get current device dpi that is used for transfer between dp and px
        listView = findViewById(R.id.listview);

        listView.post(new Runnable() {
            @Override
            public void run() {
                //this function is used to get the listview's size on running and initialize the listview
                Intent intent = getIntent();
                ArrayList<QuizResult> dataSourceList = null;

                if (intent.hasExtra("HISTORY_LIST")) {
                    //get data from intent, this way will cost less system resource than the way that every time it gets data from db
                    dataSourceList = (ArrayList<QuizResult>) intent.getSerializableExtra("HISTORY_LIST");
                    Log.i("data From Intent", "initializeListView: ");
                } else {
                    //in case, there is no data in intent, it will get data from db
                    DbHelper db = new DbHelper(QuizHistoryActivity.this);
                    try {
                        dataSourceList = db.getAllQuizResultRecordsOrderByGradeDesc();
                        Log.i("data From Database", "initializeListView: ");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        db.close();
                    }
                }
                double tempNumber;
                //get listview's height and transfer to dp unit
                availableListViewHeightInDp = transferPxToDp(currentDPI, listView.getHeight() - LISTVIEW_MARGIN_PX);
                if (isHorizontalScreen()) {
                    currentOrientation = FLAG_HORITAL;
                    tempNumber = HORITAL_ITEMS_NUMBER;
                    bestItemHeightInDp = availableListViewHeightInDp / tempNumber;//determine the best size of one item to make sure that entire list view only occupy available size of screen
                    customListViewAdapter = new CustomListViewAdapter(HORITAL_ITEMS_NUMBER, dataSourceList);
                } else {
                    currentOrientation = FLAT_VERTICAL;
                    tempNumber = VERTICAL_ITEMS_NUMBER;
                    bestItemHeightInDp = availableListViewHeightInDp / tempNumber;
                    customListViewAdapter = new CustomListViewAdapter(VERTICAL_ITEMS_NUMBER, dataSourceList);
                }
                lastPage = customListViewAdapter.getLastPage();//last page will be used to determine if current's page is last page
                Log.d("lasgpage", "run: " + lastPage);
                customListViewAdapter.switchToPage(currentPage);
                listView.setAdapter(customListViewAdapter);
                updateButtons();
            }
        });
        tvEmail = findViewById(R.id.tvEmail);
        tvDisplayPageNumber = findViewById(R.id.tvDisplayPageNumber);
        historyContraintLayout = findViewById(R.id.myConstraintLayout);
        if (savedInstanceState != null) {
            int previousPage = savedInstanceState.getInt("PreviousPage");
            int previousOrientation = savedInstanceState.getInt("PreviousOrientation");
            if (previousOrientation == FLAT_VERTICAL && isHorizontalScreen()) {
                currentPage = convertVerticalPageToHoriticalPage(previousPage);
            } else if (previousOrientation == FLAG_HORITAL && !isHorizontalScreen()) {
                currentPage = convertHoriticalPageToVerticalPage(previousPage);
            } else {
                currentPage = previousPage;
            }
        }
        // Set title
        setTitle("Quiz History");
    }

    private int convertVerticalPageToHoriticalPage(int pageNumber) {
        //if the screen rotate from vertical page to horitical page , it will dynamically caculate the best page that the activity will display the data.
        return pageNumber * VERTICAL_ITEMS_NUMBER / HORITAL_ITEMS_NUMBER;
    }

    private int convertHoriticalPageToVerticalPage(int pageNumber) {
        return pageNumber * HORITAL_ITEMS_NUMBER / VERTICAL_ITEMS_NUMBER;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("PreviousPage", currentPage);
        outState.putInt("PreviousOrientation", currentOrientation);
        super.onSaveInstanceState(outState);
    }

    public int getScreenOrientation() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        int orientation = Configuration.ORIENTATION_UNDEFINED;
        if (width == height) {
            orientation = Configuration.ORIENTATION_SQUARE;
        } else {
            if (width < height) {
                orientation = Configuration.ORIENTATION_PORTRAIT;
            } else {
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation;
    }
//check the current screen orientation
    private boolean isHorizontalScreen() {
        int orientation = getScreenOrientation();
        switch (orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                return false;
            case Configuration.ORIENTATION_LANDSCAPE:
                return true;
            default:
                return false;
        }
    }

    private int transferPxToDp(int dpi, int px) {
        return (px * BASELINE_MDPI) / dpi;
    }

    private int transferDpToPx(int dpi, int dp) {
        return dp * dpi / BASELINE_MDPI;
    }

    private void updateData() {
        customListViewAdapter.switchToPage(currentPage);
        customListViewAdapter.notifyDataSetChanged();
        updateButtons();
    }

    private void updateButtons() {
        if (lastPage == FIRST_PAGE) {
            next.setEnabled(false);
            previous.setEnabled(false);
        } else if (currentPage == FIRST_PAGE) {
            next.setEnabled(true);
            previous.setEnabled(false);
            tvDisplayPageNumber.setText((currentPage + 1) + "");
        } else if (currentPage == lastPage) {
            next.setEnabled(false);
            previous.setEnabled(true);
            tvDisplayPageNumber.setText((currentPage + 1) + "");
        } else {
            next.setEnabled(true);
            previous.setEnabled(true);
            tvDisplayPageNumber.setText((currentPage + 1) + "");
        }
    }
//This adapter is used for display history record in a listview
    class CustomListViewAdapter extends BaseAdapter {

        private int itemsPerPage, lastPageItems, lastPage, currentStartPosition = 0, currentItemNumber;
        private ArrayList<QuizResult> dataSource;

        public CustomListViewAdapter(int itemsPerPage, ArrayList<QuizResult> dataSource) {
            this.itemsPerPage = itemsPerPage;
            this.dataSource = dataSource;
            int totalItems = dataSource.size();

            if (totalItems != 0) {
                this.lastPage = (totalItems - 1) / itemsPerPage;
                this.lastPageItems = totalItems % itemsPerPage == 0 ? itemsPerPage : totalItems % itemsPerPage;
            } else {
                this.lastPageItems = 0;
                this.lastPage = 0;
            }
        }

        public void switchToPage(int currentPage) {
            currentStartPosition = currentPage * itemsPerPage;
            if (currentPage == lastPage) {
                currentItemNumber = lastPageItems;
            } else {
                currentItemNumber = itemsPerPage;
            }
        }

        public int getLastPage() {
            return lastPage;
        }

        @Override
        public int getCount() {
            return currentItemNumber;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.custom_history_item_layout, null);
            ImageView ivRank = convertView.findViewById(R.id.ivRank);
            TextView tvEmail = convertView.findViewById(R.id.tvEmail);
            TextView tvGrade = convertView.findViewById(R.id.tvGrade);
            RelativeLayout myItemLayout = convertView.findViewById(R.id.myItemLayout);
            int bestItemInpx = transferDpToPx(currentDPI, (int) Math.floor(bestItemHeightInDp));
            myItemLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, bestItemInpx));
            QuizResult qr = dataSource.get(currentStartPosition + position);
            if (currentStartPosition == 0) {
                switch (position) {
                    case FIRST_PLACE:
                        ivRank.setImageResource(R.mipmap.first);
                        break;
                    case SECOND_PLACE:
                        ivRank.setImageResource(R.mipmap.second);
                        break;
                    case THIRD_PLACE:
                        ivRank.setImageResource(R.mipmap.third);
                        break;
                    default:
                        ivRank.setImageResource(R.mipmap.up);
                }
            } else {
                ivRank.setImageResource(R.mipmap.down);
            }
            tvEmail.setText(qr.getEmail());
            tvGrade.setText(qr.getGrade() + "");
            return convertView;
        }
    }
}
